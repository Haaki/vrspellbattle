﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//-------------------------------
//  Unused legacy from gamejam
//-------------------------------

public class Charged : NetworkBehaviour
{
    Spell spell;
    float castTime;

    private void Awake()
    {
        spell = GetComponent<Spell>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
            Hold();
        if (Input.GetButtonUp("Fire1"))
            Release();
    }

    void Hold()
    {
        castTime = Time.time;
        spell.chargeEffect.GetComponent<ParticleSystem>().Play();
        spell.isAiming = true;
    }
    void Release()
    {
        spell.chargeEffect.GetComponent<ParticleSystem>().Stop();
        spell.isAiming = false;
        if (Time.time - castTime > spell.castingTime)
        {
            if (spell.modifiers.Contains(Spell.Modifier.Multiple))
            {
                int i = 1;
                while (Time.time - castTime > spell.castingTime * i)
                    i++;
                spell.multiCharge = Mathf.Min(i, spell.multiAmount);
            }
            if (spell.targeting != Spell.Targeting.Entity || spell.target != null)
                spell.Cast();
        }
    }
}

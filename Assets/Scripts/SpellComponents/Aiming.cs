﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//-------------------------------
//  Unused legacy from gamejam
//-------------------------------

public class Aiming : NetworkBehaviour
{
    Spell spell;
    public ParticleSystem aimPs;

    private void Awake()
    {
        spell = GetComponent<Spell>();
        if (spell.targeting != Spell.Targeting.Self)
        {
            aimPs = Instantiate(Resources.Load<GameObject>("Prefabs/Particle Effects/AimParticles"), spell.aimingTransform).GetComponent<ParticleSystem>();
            if (spell.targeting == Spell.Targeting.Entity)
            {
                aimPs.transform.Find("Twinkle").gameObject.SetActive(false);
            }
            var volt = aimPs.velocityOverLifetime;
            volt.z = spell.arcSpeed;
            if (spell.targeting == Spell.Targeting.Arc)
                aimPs.gravityModifier = 1;
        }
    }
    private void Update()
    {
        if (spell.isAiming)
        {

            if (aimPs.isStopped)
                aimPs.Play();
            RaycastHit rayHit;


            if (spell.targeting == Spell.Targeting.Point || spell.targeting == Spell.Targeting.Entity)
                if (Physics.Raycast(new Ray(transform.position, transform.forward), out rayHit, spell.pointRange))
                {
                    if (spell.targeting == Spell.Targeting.Entity)
                        spell.target = rayHit.transform;
                }
                else
                {
                    if (aimPs.isPlaying)
                    {
                        aimPs.Stop();
                        aimPs.Clear();
                    }
                }
        }
        if (!spell.isAiming && aimPs.isPlaying)
        {
            aimPs.Stop();
            aimPs.Clear();
        }
    }
}

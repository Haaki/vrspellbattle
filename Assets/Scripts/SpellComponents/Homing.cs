﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Homing : NetworkBehaviour
{
    public Transform targetTransform;
    float homingAmount;
    Projectile projectile;
    Spell spell;
    Rigidbody rb;
    bool clearPath = false;
    float lastObstacleTime;
    float startTime = 0;
    Vector3 clearDirection;

    private void Awake()
    {
        startTime = Time.time;
        rb = GetComponent<Rigidbody>();
        projectile = GetComponent<Projectile>();
        spell = projectile.spell;
        lastObstacleTime = Time.time + spell.homingDelay;
        homingAmount = spell.homingAmount;
    }
    private void Start()
    {
        clearDirection = rb.velocity;
    }

    private void FixedUpdate()
    {
        if (rb == null)
            Destroy(this);
        else if (spell.target)
        {
            targetTransform = spell.target;
            float distRatio = Mathf.Min(1, (spell.target.position - transform.position).magnitude / (spell.target.position - spell.caster.transform.position).magnitude);
            transform.LookAt(spell.target);

            RaycastHit rayHit;

            if (Physics.Raycast(new Ray(transform.position, rb.velocity), out rayHit, 5))
            {
                if (rayHit.transform != spell.target && !rayHit.transform.GetComponent<Projectile>())
                {
                    clearPath = false;
                    lastObstacleTime = Time.time;
                    if (Physics.Raycast(new Ray(transform.position, clearDirection), out rayHit, 5))
                    {
                        if (rayHit.transform != spell.target && !rayHit.transform.GetComponent<Projectile>())
                            clearDirection = new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f);
                    }
                }
            }
            //Debug.DrawRay(transform.position, clearDirection.normalized * 10, ((clearPath)? Color.green : Color.red));

            if (Time.time > lastObstacleTime + spell.homingDelay)
            {
                clearPath = true;
                clearDirection = transform.forward;
            }
            clearDirection += new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * homingAmount / 2;
            rb.velocity = Vector3.RotateTowards(rb.velocity, clearDirection, rb.velocity.magnitude * homingAmount * ((clearPath)?1:10) * (1 / distRatio) * Time.fixedDeltaTime, 0);
            

            rb.velocity += rb.velocity * spell.homingSpeedup * Time.fixedDeltaTime;
            homingAmount += homingAmount * spell.homingSpeedup * Time.fixedDeltaTime;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Projectile : NetworkBehaviour
{
    public Spell spell;
    public bool launched = false;
    bool stopped = false;
    float lastLaunch;
    int stored;
    int penetrations = 0;
    int jumped = 0;
    public float launchTime;
    Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        spell = GetComponent<Spell>();
    }
    private void Start()
    {
        if (launched)
            spell.projectileEffect.GetComponent<ParticleSystem>().Play();
    }

    public void Cast()
    {
        if (spell.modifiers.Contains(Spell.Modifier.Multiple))
        {
            stored += spell.multiCharge;
            lastLaunch = Time.time;
        }
        else
            Launch();
    }

    private void Update()
    {
        if (stored > 0)
            if (Time.time - lastLaunch > spell.multiInterval)
            {
                Launch();
                lastLaunch = Time.time;
                stored--;
            }
        if (rb) transform.LookAt(transform.position + rb.velocity);
    }

    private void LateUpdate()
    {
        if (stopped && spell.projectileEffect.GetComponent<ParticleSystem>().isPlaying)
            spell.projectileEffect.GetComponent<ParticleSystem>().Stop();
    }
    private void FixedUpdate()
    {
        if (rb == null) rb = GetComponent<Rigidbody>();
        if (launched && !stopped)
        {
            RaycastHit rayHit;
            if (Physics.Raycast(new Ray(transform.position, rb.velocity), out rayHit, rb.velocity.magnitude * Time.fixedDeltaTime * 1.5f))
                if (!rayHit.collider.GetComponent<Projectile>())
                    if (!spell.modifiers.Contains(Spell.Modifier.Piercing) || (penetrations >= spell.maxPierce && spell.maxPierce > 0) || rayHit.collider.GetComponent<Damageable>() == null)
                    {
                        transform.position = rayHit.point;
                        rb.velocity = Vector3.zero;
                    }
        }
    }
    void Launch()
    {
        if (!(spell.targeting == Spell.Targeting.Entity) || spell.target)
            spell.caster.GetComponent<Caster>().CmdSpawnSpell(name, spell.caster);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Projectile>())
        {
            if (spell.modifiers.Contains(Spell.Modifier.Damaging))
            {
                if (spell.aoe)
                    foreach (Collider hit in Physics.OverlapSphere(transform.position, spell.aoeRadius))
                        hit.GetComponent<Damageable>()?.DealDamage(spell.damage * ((spell.aoeFalloff) ? 1 - (transform.position - hit.transform.position).magnitude / spell.aoeRadius : 1), spell.element);
                else
                {
                    other.GetComponent<Damageable>()?.DealDamage(spell.damage, spell.element);
                }
                if (spell.force)
                {
                    foreach (Collider hit in Physics.OverlapSphere(transform.position, spell.forceRadius))
                        if (hit.GetComponent<Rigidbody>() && hit.gameObject != gameObject)
                            hit.GetComponent<Rigidbody>().AddForce(((hit.transform.position - transform.position).normalized * ((transform.position - hit.transform.position).magnitude / spell.forceRadius)) * spell.forceAmount);
                }
            }
            if (spell.modifiers.Contains(Spell.Modifier.Piercing) && !(penetrations >= spell.maxPierce && spell.maxPierce > 0) && other.GetComponent<Damageable>() != null)
            {
                //Debug.Log($"{name} penetrated {other.gameObject.name}");
                penetrations++;
            }
            else if (spell.modifiers.Contains(Spell.Modifier.Chain) && other.GetComponent<Damageable>() != null)
            {
                spell.impactEffect.GetComponent<ParticleSystem>().Play();
                if (jumped >= spell.chainJumps && spell.chainJumps > 0)
                {
                    rb.velocity = Vector3.zero;
                    Destroy(GetComponent<Rigidbody>());
                    Destroy(GetComponent<Collider>());
                    transform.SetParent(other.transform, true);
                    Destroy(gameObject, 5);
                    stopped = true;
                }
                else
                {
                    foreach (Collider hit in Physics.OverlapSphere(transform.position, spell.jumpRange))
                    {
                        if (hit.GetComponent<Damageable>())
                        {
                            //rb.velocity = transform.position - hit. //Swapping PC

                        }
                    }
                }
            }
            else
            {
                //Debug.Log($"{name} hit {other.gameObject.name}");
                rb.velocity = Vector3.zero;
                Destroy(GetComponent<Rigidbody>());
                Destroy(GetComponent<Collider>());
                spell.impactEffect.GetComponent<ParticleSystem>().Play();
                transform.SetParent(other.transform, true);
                Destroy(gameObject, 5);
                stopped = true;
            }

        }
    }
}

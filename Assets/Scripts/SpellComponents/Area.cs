﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
    public Spell spell; 

    public bool launched = false;   //  If this is an active spell
    public bool summoned = false;   //  If a minion has been summoned
    public float lastLaunch = 0;    //  Last timestamp the spell was launched
    int stored = 0;                 //  Used for the multiple modifier
    float lastTick = 0;             //  Last time the spell did something


    private void Awake() {

        spell = GetComponent<Spell>();  //  Get a reference to the spell this is a part of
    }

    private void Start()
    {
        if (launched && spell.spellType != Spell.SpellType.Minion)
        {
            if (spell.areaEffect != null)  
                spell.areaEffect.GetComponent<ParticleSystem>().Play(); //  Activate visual effects
            if (!spell.modifiers.Contains(Spell.Modifier.Lingering))
                DoStuff();                                              //  Do the spells effects
        }
    }

    private void Update()
    {
        //  Spaws an elemental if not already spawned
        if (spell.spellType == Spell.SpellType.Minion && launched && !summoned) 
        {
            SpawnElemental();
            summoned = true;
        }
        //  Repeats spell effect if it is lingering
        if (spell.modifiers.Contains(Spell.Modifier.Lingering))
            if(lastTick + spell.tickRate > Time.time)
            {
                DoStuff();
                lastTick = Time.time;
            }
    }
    //  Activate the spell
    void Launch()   
    {
        spell.caster.GetComponent<Caster>().CmdSpawnSpell(name, spell.caster);
    }

    //  The spells effects
    public void DoStuff() {
        DealDamage();   
        //  For wall spells
        if (spell.modifiers.Contains(Spell.Modifier.Wall)) {
            transform.localScale = Vector3.one * 1f;
            transform.position += Vector3.up * 0.6f * transform.localScale.y;
            gameObject.AddComponent<WallOrganizer>().walls = new GameObject[spell.wallDimensions.x, spell.wallDimensions.y, spell.wallDimensions.z];
            transform.eulerAngles = new Vector3(0, spell.caster.GetComponent<Caster>().aimTransform.eulerAngles.y, 0);
        }
    }

    //  Deals damage in the effected area
    public void DealDamage()
    {
        foreach (Collider hit in Physics.OverlapSphere(transform.position, spell.aoeRadius))
        {
            hit.GetComponent<Damageable>()?.DealDamage(spell.damage * ((spell.aoeFalloff) ? 1 - (transform.position - hit.transform.position).magnitude / spell.aoeRadius : 1), spell.element);
        }
    }

    //  Casts the spell
    public void Cast()
    {
        if (spell.modifiers.Contains(Spell.Modifier.Multiple))
        {
            stored += spell.multiCharge;
            lastTick = Time.time;
        }
        else
            Launch();
    }

    //  Same as launch but for elementals
    public void SpawnElemental()
    {
        spell.caster.GetComponent<Caster>().CmdSummonCreature(spell.gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//-------------------------------
//  Unused legacy from gamejam
//-------------------------------

public class Channeled : NetworkBehaviour
{
    Spell spell;
    float lastCast = 0;

    private void Awake()
    {
        spell = GetComponent<Spell>();
    }
    private void Update()
    {
        if (Input.GetButton("Fire1"))
            Hold();
        if (Input.GetButtonUp("Fire1"))
            Release();
    }
    void Hold()
    {
        spell.isAiming = false;
        if (Time.time - lastCast > spell.castingTime)
        {
            if (spell.targeting != Spell.Targeting.Entity || spell.target != null)
                spell.Cast();
            lastCast = Time.time;
        }
    }
    void Release()
    {
        if (spell.targeting == Spell.Targeting.Entity)
            spell.isAiming = true;
    }
}

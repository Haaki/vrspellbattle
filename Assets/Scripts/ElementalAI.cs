﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class ElementalAI : MonoBehaviour
{
    public State<ElementalAI> newState; //The next state the elemental should have
    public float speed = 5;             //the speed of the elemental
    public List<GameObject> enemies;        //the enemies of the elemental
    public StateMachine<ElementalAI> stateMachine { get; set; } //the statemachine 
    public Caster caster;               //caster script, allows an object to cast spells
    public string[] spells;             //the spells the elemental can use
    public int target = 0;              //the index of the current target
    public float holdTimer = -1;        //timer of holding the spell
    public float moveChance = 0.4f;     //the chance of the elemental wanting to move
    public float moveChanceMult = 1.1f; //the multiplier of the move chance

    // Use this for initialization
    void Start ()
    {   //inits the statemachine and sets first state to choose spell state
        stateMachine = new StateMachine<ElementalAI>(this); 
        stateMachine.ChangeState(ChooseSpellState.Instance);
        caster = GetComponent<Caster>();    //inits the caster object
    }
	
	// Update is called once per frame
	void Update ()
    {   //checks if the elemental is dead 
        if (GetComponent<Damageable>().health <= 0)
            newState = DeadState.Instance;  //sets the next state to be the dead state
        stateMachine.Update();      //updates the statemachine
	}
}

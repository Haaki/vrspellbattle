﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Spell : NetworkBehaviour
{
    public enum Element { Fire, Ice, Lightning, Arcane, Radiant, Necrotic, Poison, Acid, Earth }
    public enum CastingType { Charge, Channel }
    public enum PowerLevel { Cantrip, Regular, Heavy }
    public enum Targeting { Point, Arc, Entity, Self }
    public enum SpellType { Projectile, Cone, Target, Area, Minion }
    public enum Modifier { Damaging, Homing, Piercing, Multiple, Chain, Wall, Debuff, Dot, Overcharge, Bouncy, Lingering }
    public enum Debuff { Slow, Root, Poison, Blind }

    public GameObject caster;

    public Element element;

    public CastingType castingType;
    public float manaCost = 10;
    public float castingTime = 0.5f;
    public GameObject chargeEffect;


    public PowerLevel powerLevel;

    public Targeting targeting;
    public Transform aimingTransform;
    public bool isAiming;
    public Vector3 rotation;
    //Point
    public float pointRange;
    //Arc
    public float arcSpeed;
    //Entity
    public Transform target;
    //Self

    public SpellType spellType;
    //Projectile
    public float projectileSpeed;
    public float decay = 10;
    public float projectileSize;
    public GameObject projectileEffect;
    public GameObject impactEffect;
    //Cone
    public float coneRange;
    public float spread;
    //Target
    //Area
    public float areaAccuracy;
    public bool grounded;
    public GameObject areaEffect;
    //Minion
    public GameObject minionPrefab;

    public List<Spell.Modifier> modifiers;
    //Damaging
    public float damage;
    public bool aoe;
    public float aoeRadius;
    public bool aoeFalloff;
    public bool force;
    public float forceRadius;
    public float forceAmount;
    //Dot
    public float dot;
    public float dotTime;
    //Debuff
    public Debuff debuff;
    public float debuffTime;
    //Homing
    public float homingAmount;
    public float homingSpeedup;
    public float homingDelay;
    //Multiple
    public int multiAmount;
    public int multiCharge;
    public float multiInterval;
    //Chain
    //public int chainAmount;
    public int chainJumps;
    public float jumpRange;
    //Piercing
    public int maxPierce;
    //Wall
    public Vector3Int wallDimensions;
    //Overcharges
    public float chargeThreshold;
    public GameObject overchargedSpell;
    //Bouncy
    //Lingering
    public float lingerTime;
    public float tickRate;

	void Start()
    {
        if (modifiers == null)
            modifiers = new List<Modifier>();
        if (aimingTransform == null)
            aimingTransform = transform;
        if (spellType == SpellType.Projectile)
            arcSpeed = projectileSpeed;
    }

    public void Cast()
    {
        if (spellType == SpellType.Projectile)
            if (GetComponent<Projectile>())GetComponent<Projectile>().Cast();
            else gameObject.AddComponent<Projectile>().Cast();
        else if (spellType == SpellType.Area || spellType == SpellType.Minion)
            if (GetComponent<Area>()) GetComponent<Area>().Cast();
            else gameObject.AddComponent<Area>().Cast();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIelement : MonoBehaviour
{
    public Sprite icon;
    GameObject iconGO;
    public string text = "DEFAULT";
    GameObject textGO;
    public Material background;

    public bool selected = false;
    public bool active = true;
    public float extraSpacing = 0;

    public List<GameObject> subLists = new List<GameObject>();
    public Vector3 scale;
    public int pos = 0;     //  Position in UI
    public int subPos = -1; //  Position in sub-menu
    

    // Use this for initialization
    void Start()
    {
        scale = transform.localScale;
        gameObject.AddComponent<BoxCollider>();
        gameObject.GetComponent<Renderer>().material = background;
        gameObject.tag = "UI";

        //  Optional Icon
        if (icon != null)
        {
            iconGO = new GameObject(name + "_icon");
            iconGO.AddComponent<SpriteRenderer>().sprite = icon;
            iconGO.transform.parent = transform;
            iconGO.transform.localEulerAngles = new Vector3(0,0,90);
            iconGO.transform.localPosition = new Vector3((-transform.localScale.x / 2) * .1f, 0, -transform.localScale.z * 101);
            iconGO.transform.localScale = transform.localScale * .5f;
        }

        //  Text Element
        textGO = new GameObject(name + "_text");
        textGO.transform.parent = transform;
        textGO.transform.localEulerAngles = new Vector3(0, 0, 0);
        textGO.transform.localPosition = new Vector3(0, 0, -transform.localScale.z * 10);
        textGO.transform.localScale = new Vector3(1f/transform.localScale.x, 1f/transform.localScale.y, 1f) * .1f;
        textGO.AddComponent<Canvas>();
        textGO.AddComponent<TextMesh>().text = text;
        textGO.GetComponent<TextMesh>().alignment = TextAlignment.Center;
        textGO.GetComponent<TextMesh>().anchor = TextAnchor.MiddleCenter;
        textGO.GetComponent<TextMesh>().fontSize = 75;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
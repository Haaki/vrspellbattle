﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class InGameUI : MonoBehaviour
{
    public List<GameObject> mainList = new List<GameObject>();
    public Vector3 position;
    public int focus = 1;
    public int subFocus = -1;
    public GameObject cube;
    public Material background;
    float verticalSpacing = .1f;
    float UIspeed = 16f;
    Vector3 lastPos = Vector3.zero;

    Color activeColor = Color.grey;
    Color inactiveColor = Color.black;
    Color selectedColor = Color.white;

    public List<Sprite> icons = new List<Sprite>();

    public int[] loadOut = new int[4];

    public GameObject pointer;
    public NetworkManager net;

    // Use this for initialization
    void Start ()
    {
        PopulateUI();
        position = transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (mainList.Count > 1)
        {
            ListDisplay();

            //  if all spells have been selected
            if (CheckIfReady())
            {
                if (Input.GetAxis("Jump") > .1f || Input.GetAxis("Fire1") > .1f)
                {
                    for (int i=0; i<4; i++)
                        GameObject.Find("Game Manager").GetComponent<GameController>().loadOuts[0,i] = loadOut[i];
                    net.StartHost();    //  Load arena
                }
            }
        }
	}

    //  Add UI elements to the list
    void PopulateUI()
    {
        CreateElement(ref mainList, false, "LOADOUT", false, null, background, new Vector2(10, 3));
        CreateElement(ref mainList, false, "Basic spells", true, new List<GameObject>(), background, new Vector2(10, 2));
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Firebolt", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Magic Missile", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Ice Knife", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Acid Rain", true, null, background, new Vector2(8, 1), icons[0]);
        CreateElement(ref mainList, false, "Advanced spells", true, new List<GameObject>(), background, new Vector2(10, 2));
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Chain Lightning", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Fireball", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Colour Spray", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Prismatic Spray", true, null, background, new Vector2(8, 1), icons[0]);
        CreateElement(ref mainList, false, "Utility", true, new List<GameObject>(), background, new Vector2(10, 2));
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Wall of Ice", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Wall of Stone", true, null, background, new Vector2(8, 1), icons[0]);
            //CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Utility", true, null, background, new Vector2(8, 1), icons[0]);
            //CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Utility", true, null, background, new Vector2(8, 1), icons[0]);
        CreateElement(ref mainList, false, "Elemental", true, new List<GameObject>(), background, new Vector2(10, 2));
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Arcane", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Fire", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Necrotic", true, null, background, new Vector2(8, 1), icons[0]);
            CreateElement(ref (mainList[mainList.Count-1].GetComponent<UIelement>().subLists), true, "Ice", true, null, background, new Vector2(8, 1), icons[0]);

    }

    //  Create UI element
    void CreateElement(ref List<GameObject> list, bool subList, string text, bool active, List<GameObject> subLists, Material mat, Vector2 size, Sprite icon = null)
    {
        GameObject go = Instantiate(cube, transform);
        go.name = "menu_" + text;
        go.AddComponent<UIelement>().text = text;
        go.GetComponent<UIelement>().active = active;
        go.GetComponent<UIelement>().subLists = subLists;
        go.GetComponent<UIelement>().background = mat;
        go.GetComponent<UIelement>().icon = icon;
        go.transform.localScale = new Vector3(size.x, size.y, 0.01f);

        if (mainList.Count == 0)
            go.transform.localPosition = Vector3.zero;
        else
        {
            GameObject above = mainList[mainList.Count - 1];
            go.transform.localPosition = 
                above.transform.localPosition 
                - new Vector3(0, (above.transform.localScale.y/2 + go.transform.localScale.y/2) + verticalSpacing * transform.localScale.y, 0);
        }
        list.Add(go);
        go.GetComponent<UIelement>().pos = mainList.Count - 1;
        if (subList)
            go.GetComponent<UIelement>().subPos = mainList[mainList.Count - 1].GetComponent<UIelement>().subLists.Count - 1;
    }

    //  Displays and moves list items, handles input
    void ListDisplay()
    {
        //  List input
        int layerMask = 1 << LayerMask.NameToLayer("Player");
        layerMask = ~layerMask;
        RaycastHit hit;
        if (Physics.Raycast(new Ray(pointer.transform.position, pointer.transform.forward), out hit, 100, layerMask)) {

            Transform objectHit = hit.transform;

            if (objectHit.tag == "UI") {
                focus = objectHit.GetComponent<UIelement>().pos;
                if (objectHit.GetComponent<UIelement>().subPos != -1) {
                    subFocus = objectHit.GetComponent<UIelement>().subPos;
                    if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire2")) {
                        for (int j = 0; j < mainList[focus].GetComponent<UIelement>().subLists.Count; j++)
                            mainList[focus].GetComponent<UIelement>().subLists[j].GetComponent<UIelement>().selected = false;

                        objectHit.GetComponent<UIelement>().selected = !objectHit.GetComponent<UIelement>().selected;
                    }
                }
                else {
                    subFocus = -1;
                    if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire2"))
                        objectHit.GetComponent<UIelement>().selected = !objectHit.GetComponent<UIelement>().selected;
                }

                lastPos = hit.point;
            }
        }

        focus = Mathf.Clamp(focus, 1, mainList.Count - 1);
        if (subFocus != -1)
            subFocus = Mathf.Clamp(subFocus, 0, mainList[focus].GetComponent<UIelement>().subLists.Count - 1);

        //  Menu scrolling
        float pos = transform.position.y;
        float scrollLimit = 3.0f;
        if (Mathf.Abs(position.y - lastPos.y) > scrollLimit * transform.localScale.y)
            pos = transform.position.y - lastPos.y;

        //  Move
        transform.position = Vector3.Lerp(
                transform.position,
                new Vector3(
                    0,
                    pos,
                    0),
                Time.deltaTime * UIspeed / 8);

        //  Lowest value
        float lowest = mainList[mainList.Count - 1].transform.position.y;
        if (mainList[mainList.Count - 1].GetComponent<UIelement>().subLists != null)
            if (mainList[mainList.Count - 1].GetComponent<UIelement>().selected)
                lowest = mainList[mainList.Count - 1].GetComponent<UIelement>().subLists[mainList[mainList.Count - 1].GetComponent<UIelement>().subLists.Count - 1].transform.position.y;

        //  Clamp position
        transform.position = new Vector3(
            transform.position.x,
            Mathf.Clamp(transform.position.y, position.y, position.y + (transform.position.y - lowest)),
            transform.position.z);

        //  Debug lines
        Debug.DrawLine(new Vector3(-10, position.y, 0), new Vector3(10, position.y, 0));
        Debug.DrawLine(new Vector3(-10, transform.position.y, 0), new Vector3(10, transform.position.y, 0), Color.red);
        Debug.DrawLine(new Vector3(-10, position.y + scrollLimit, 0), new Vector3(10, position.y + scrollLimit, 0), Color.blue);
        Debug.DrawLine(new Vector3(-10, position.y - scrollLimit, 0), new Vector3(10, position.y - scrollLimit, 0), Color.blue);
        
        Debug.DrawLine(new Vector3(-10, lowest, 0), new Vector3(10, lowest, 0), Color.green);


        //  Lists
        for (int i = 0; i < mainList.Count; i++)
        {
            mainList[i].GetComponent<UIelement>().extraSpacing = 0;

            //  Sub Lists
            if (mainList[i].GetComponent<UIelement>().selected && mainList[i].GetComponent<UIelement>().subLists != null)
            {
                for (int j = 0; j < mainList[i].GetComponent<UIelement>().subLists.Count; j++)
                {
                    mainList[i].GetComponent<UIelement>().extraSpacing += mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale.y + verticalSpacing * transform.localScale.y;

                    //  Scale
                    mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale = Vector3.Lerp(
                        mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale,
                        mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<UIelement>().scale,
                        Time.deltaTime * UIspeed
                    );

                    //  Spacing
                    float subSpacing = 0;
                    GameObject above;
                    if (j > 0)
                        above = mainList[i].GetComponent<UIelement>().subLists[j - 1];
                    else
                        above = mainList[i];

                    subSpacing =
                                above.transform.localPosition.y
                                - ((above.transform.localScale.y / 2 + mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale.y / 2) + verticalSpacing * transform.localScale.y);

                    //  Position
                    mainList[i].GetComponent<UIelement>().subLists[j].transform.localPosition = Vector3.Lerp(
                        mainList[i].GetComponent<UIelement>().subLists[j].transform.localPosition,
                        new Vector3(
                            (i == focus && j == subFocus) ? 2 : 1,
                            subSpacing,
                            0),
                        Time.deltaTime * UIspeed);
                }
            }
            else // Closed submenu
            {
                if (mainList[i].GetComponent<UIelement>().subLists != null)
                    for (int j = 0; j < mainList[i].GetComponent<UIelement>().subLists.Count; j++)
                    {
                        mainList[i].GetComponent<UIelement>().subLists[j].transform.position = Vector3.Lerp(
                            mainList[i].GetComponent<UIelement>().subLists[j].transform.position,
                            mainList[i].transform.position,
                            Time.deltaTime * UIspeed
                            );
                        mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale = Vector3.Lerp(
                            mainList[i].GetComponent<UIelement>().subLists[j].transform.localScale,
                            Vector3.zero,
                            Time.deltaTime * UIspeed
                        );
                    }
            }

            //  Main List

            //  Spacing
            float spacing = 0;
            if (i > 0)
            {
                GameObject above = mainList[i - 1];
                spacing =
                    above.transform.localPosition.y
                    - ((above.transform.localScale.y / 2 + mainList[i].transform.localScale.y / 2) + verticalSpacing * transform.localScale.y);
            }

            //  Position
            mainList[i].transform.localPosition = Vector3.Lerp(
                mainList[i].transform.localPosition,
                new Vector3(
                    (i == focus && subFocus == -1) ? 1 : 0,
                    spacing - ((i == 0) ? 0 : mainList[i - 1].GetComponent<UIelement>().extraSpacing),
                    0),
                Time.deltaTime * UIspeed);
        }

        //  Visuals
        for (int i = 0; i < mainList.Count; i++)
        {
            if (mainList[i].GetComponent<UIelement>().active)
            {
                /*if (mainList[i].GetComponent<UIelement>().selected)
                    mainList[i].GetComponent<Renderer>().material.color = selectedColor;
                else*/
                    mainList[i].GetComponent<Renderer>().material.color = activeColor;
            }
            else
            {
                mainList[i].GetComponent<Renderer>().material.color = inactiveColor;
            }

            if (mainList[i].GetComponent<UIelement>().subLists != null)
                for (int j = 0; j < mainList[i].GetComponent<UIelement>().subLists.Count; j++)
                {
                    if (mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<UIelement>().active)
                    {
                        if (mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<UIelement>().selected)
                            mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<Renderer>().material.color = selectedColor;
                        else
                            mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<Renderer>().material.color = activeColor;
                    }
                    else
                    {
                        mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<Renderer>().material.color = inactiveColor;
                    }
                }
        }
    }

    //  Check if all 4 spell slots have been filled
    bool CheckIfReady()
    {
        int selected = 0;

        for (int i = 0; i < mainList.Count; i++)
            if (mainList[i].GetComponent<UIelement>().subLists != null)
                for (int j = 0; j < mainList[i].GetComponent<UIelement>().subLists.Count; j++)
                    if (mainList[i].GetComponent<UIelement>().subLists[j].GetComponent<UIelement>().selected)
                    {
                        loadOut[i-1] = j;
                        selected++;
                        continue;
                    }

        if (selected == 4)
            return true;

        return false;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpellLoader : NetworkBehaviour
{
    NetworkManager nm;

    private void Awake()
    {
        nm = GetComponent<NetworkManager>();
        nm.spawnPrefabs.AddRange(Resources.LoadAll<GameObject>("Prefabs/Spells/"));
        foreach (GameObject spell in nm.spawnPrefabs.FindAll(a => a.GetComponent<Spell>()?.minionPrefab))
        {
            nm.spawnPrefabs.Add(spell.GetComponent<Spell>().minionPrefab);
        }
        nm.spawnPrefabs.AddRange(Resources.LoadAll<GameObject>("Prefabs/ParticleEffects/ChargeEffects/"));
    }
}

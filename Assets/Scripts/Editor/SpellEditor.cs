﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(Spell))]
public class SpellEditor : Editor
{
    bool addModifier = false;
    List<bool> removeModifier = new List<bool>();
    public override void OnInspectorGUI()
    {
        var spell = target as Spell;

        spell.element = (Spell.Element)EditorGUILayout.EnumPopup("Element", spell.element);
        spell.castingType = (Spell.CastingType)EditorGUILayout.EnumPopup("Casting type", spell.castingType);
        if (spell.castingType == Spell.CastingType.Charge)
        {
            MakeField("Mana Cost", ref spell.manaCost);
            MakeField("Casting Time", ref spell.castingTime);
            MakeField("Charge effect", ref spell.chargeEffect);
        }
        else
        {
            MakeField("Mana Drain", ref spell.manaCost);
            MakeField("Tick delay", ref spell.castingTime);
        }

        spell.powerLevel = (Spell.PowerLevel)EditorGUILayout.EnumPopup("Power level", spell.powerLevel);
        spell.targeting = (Spell.Targeting)EditorGUILayout.EnumPopup("Targeting type", spell.targeting);
        switch (spell.targeting)
        {
            case Spell.Targeting.Point:
                MakeField("Range", ref spell.pointRange);
                break;
            case Spell.Targeting.Arc:
                if (!(spell.spellType == Spell.SpellType.Projectile))
                    MakeField("Arc speed", ref spell.arcSpeed);
                break;
            case Spell.Targeting.Entity:
                MakeField("Range", ref spell.pointRange);
                break;
            case Spell.Targeting.Self:
                break;
            default:
                break;
        }
        spell.spellType = (Spell.SpellType)EditorGUILayout.EnumPopup("Spell type", spell.spellType);
        switch (spell.spellType)
        {
            case Spell.SpellType.Projectile:
                MakeField("Speed", ref spell.projectileSpeed);
                MakeField("Size", ref spell.projectileSize);
                MakeField("Spread", ref spell.spread);
                MakeField("Decay", ref spell.decay);
                MakeField("Projectile effect", ref spell.projectileEffect);
                MakeField("Impact effect", ref spell.impactEffect);
                break;
            case Spell.SpellType.Cone:
                MakeField("Range", ref spell.coneRange);
                MakeField("Angle", ref spell.spread);
                break;
            case Spell.SpellType.Target:
                break;
            case Spell.SpellType.Area:
                MakeField("Accuracy", ref spell.areaAccuracy);
                MakeField("Grounded", ref spell.grounded);
                MakeField("Decay", ref spell.decay);
                MakeField("Effect", ref spell.areaEffect);
            break;
            case Spell.SpellType.Minion:
                MakeField("Prefab", ref spell.minionPrefab);
                break;
            default:
                break;
        }


        MakeField("--Modifiers--");
        if (addModifier)
        {
            spell.modifiers.Add(Spell.Modifier.Damaging);
            addModifier = false;
        }
        while (removeModifier.Count < spell.modifiers.Count)
            removeModifier.Add(false);

        for (int i = 0; i < spell.modifiers.Count; i++)
        {
            spell.modifiers[i] = (Spell.Modifier)EditorGUILayout.EnumPopup($"Mod #{i}:", spell.modifiers[i]);
            switch (spell.modifiers[i])
            {
                case Spell.Modifier.Damaging:
                    MakeField("Amount", ref spell.damage);
                    if (spell.spellType != Spell.SpellType.Area)
                        MakeField("Area of effect", ref spell.aoe);
                    if (spell.aoe || spell.spellType == Spell.SpellType.Area)
                    {
                        MakeField("Radius", ref spell.aoeRadius);
                        MakeField("Damage falloff", ref spell.aoeFalloff);
                    }
                    spell.force = EditorGUILayout.Toggle("Adds force", spell.force);
                    if (spell.force)
                    {
                        MakeField("Radius", ref spell.forceRadius);
                        MakeField("Power", ref spell.forceAmount);
                    }
                    break;
                case Spell.Modifier.Dot:
                    MakeField("Amount", ref spell.dot);
                    MakeField("Duration", ref spell.dotTime);
                    break;
                case Spell.Modifier.Debuff:
                    MakeField("Duration", ref spell.debuffTime);
                    spell.debuff = (Spell.Debuff)EditorGUILayout.EnumPopup("Debuff", spell.debuff);
                    break;
                case Spell.Modifier.Homing:
                    MakeField("Amount", ref spell.homingAmount);
                    MakeField("Speedup", ref spell.homingSpeedup);
                    MakeField("Delay", ref spell.homingDelay);
                    break;
                case Spell.Modifier.Multiple:
                    MakeField("Amount", ref spell.multiAmount);
                    MakeField("Interval", ref spell.multiInterval);
                    break;
                case Spell.Modifier.Chain:
                    //MakeField("Amount", ref spell.chainAmount);
                    MakeField("Jumps", ref spell.chainJumps);
                    MakeField("Range", ref spell.jumpRange);
                    break;
                case Spell.Modifier.Piercing:
                    MakeField("Max penetrations", ref spell.maxPierce);
                    break;
                case Spell.Modifier.Wall:
                    MakeField("Dimensions", ref spell.wallDimensions);
                    break;
                case Spell.Modifier.Overcharge:
                    MakeField("Spell", ref spell.overchargedSpell);
                    break;
                case Spell.Modifier.Bouncy:
                case Spell.Modifier.Lingering:
                    MakeField("Time", ref spell.lingerTime);
                    MakeField("Time", ref spell.tickRate);
                    break;
                default:
                    break;
            }
            MakeField("");
            bool temp = removeModifier[i];
            MakeField("-Remove", ref temp);
            removeModifier[i] = temp;
            if (removeModifier[i])
            {
                spell.modifiers.Remove(spell.modifiers[i]);
                removeModifier.Remove(removeModifier[i]);
            }
            MakeField("---");
        }

        MakeField("Add modifier", ref addModifier);

    }
    void MakeField(string label)
    {
        EditorGUILayout.LabelField(label);
    }
    void MakeField(string label, ref int value)
    {
        value = EditorGUILayout.IntField(label, value);
    }
    void MakeField(string label, ref float value)
    {
        value = EditorGUILayout.FloatField(label, value);
    }
    void MakeField(string label, ref Vector2 value)
    {
        value = EditorGUILayout.Vector2Field(label, value);
    }
    void MakeField(string label, ref Vector2Int value)
    {
        value = EditorGUILayout.Vector2IntField(label, value);
    }
    void MakeField(string label, ref Vector3 value)
    {
        value = EditorGUILayout.Vector3Field(label, value);
    }
    void MakeField(string label, ref Vector3Int value)
    {
        value = EditorGUILayout.Vector3IntField(label, value);
    }
    void MakeField(string label, ref bool value)
    {
        value = EditorGUILayout.Toggle(label, value);
    }
    void MakeField(string label, ref GameObject value)
    {
        value = (GameObject)EditorGUILayout.ObjectField(label, value, typeof(GameObject), true);
    }
    void MakeField(string label, ref Transform value)
    {
        value = (Transform)EditorGUILayout.ObjectField(label, value, typeof(Transform), true);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class CastSpellState : State<ElementalAI>
{
    private static CastSpellState _instance;    //holds one instance of the state

    private CastSpellState()
    {   //sets instance if not set
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static CastSpellState Instance
    {   //gets instance if set, sets it if not
        get
        {
            if (_instance == null)
            {
                new CastSpellState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
        if (own.newState != null)   
        {
            own.stateMachine.ChangeState(own.newState);
        }
        else
        {
            if (own.moveChance > Random.value)  //if the chance of moving is bigger than a random value
            {
                own.moveChance = 0.4f;  //resets chance 
                own.newState = MoveState.Instance;  //and goes to move state
            }
            else
            {
                own.moveChance *= own.moveChanceMult;   //increases chance of moving
            }
            own.holdTimer -= Time.deltaTime;    //updates timer
            if (own.caster.currentSpell.castingType == Spell.CastingType.Channel) //if casting a channeling spell
            {
                if (!own.caster.holding)    //if not holding the spell
                {
                    own.holdTimer = 100;    //sets a high holding time
                    own.caster.Hold();      //starts holding the spell
                }
                else if(Random.value < 0.05f)   //small random chance to end spell early
                {
                    own.holdTimer = -1;     //ends spell timer
                    own.newState = ChooseSpellState.Instance;   //goes to change spell state
                }
            }
            else    //if not casting a channeling spell
            {
                if (!own.caster.holding)    //if not holding spell
                {   //sets timer based on spell
                    own.holdTimer = own.caster.currentSpell.castingTime * ((own.caster.currentSpell.modifiers.Contains(Spell.Modifier.Multiple)) ? 10 : 1.1f);  
                    own.caster.Hold();  //starts holding spell
                }
            }

            if (own.holdTimer < 0)  //if done holding spell
            {
                own.caster.Release();   //releases the spell
                if(own.newState == null)    //if no new state has been chosen
                {
                    if (Random.value < 0.25f)   //25% chance of choosing new spell
                    {
                        own.newState = ChooseSpellState.Instance;   //goes to choose spell state
                    }
                    else
                    {   
                        own.newState = AimState.Instance;   // goes to aiming state
                    }
                }
            }
        }
    }
}

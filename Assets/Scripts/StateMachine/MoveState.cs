﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class MoveState : State<ElementalAI>
{
    private static MoveState _instance; //holds one instance of the state


    private MoveState()
    {   //sets the instance if not already set
        if(_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static MoveState Instance
    {   //gets state if set, sets it if not
        get
        {   
            if(_instance == null)
            {
                new MoveState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
        if(own.newState != null)
        {
            own.stateMachine.ChangeState(own.newState);
        }
        else
        {
            Vector3 walkVector; //direction to move in

            walkVector = (Random.value > 0.5f) ? own.transform.right : -own.transform.right;    //randomly moves left or right
            walkVector += new Vector3((Random.value - 0.5f) * 2, 0, (Random.value - 0.5f) * 2); //with a random component in x and z 
            walkVector = Vector3.Normalize(walkVector);                                         //normalizes 
            own.GetComponent<Rigidbody>().velocity = walkVector * own.speed * (Random.value + 0.5f);    //sets velocity
            own.newState = AimState.Instance;   //goes to aiming state
        }
    }
}

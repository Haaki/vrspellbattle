﻿namespace States
{
    public class StateMachine<T>
    {
        public State<T> currentState { get; private set; }
        public T Owner;

        public StateMachine(T o)
        {   //sets owner of statemachine 
            Owner = o;
            currentState = null;
        }

        public void ChangeState(State<T> newState)
        {   //changes state to a given state
            if (currentState != null)
                currentState.ExitState(Owner);
            currentState = newState;
            currentState.EnterState(Owner);
        }

        public void Update()
        {   //updates the state
            if (currentState != null)
                currentState.UpdateState(Owner);
        }
    }

    public abstract class State<T>
    {   
        public abstract void EnterState(T own);

        public abstract void ExitState(T own);

        public abstract void UpdateState(T own);
    }
}
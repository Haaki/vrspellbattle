﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class DeadState : State<ElementalAI>
{
    private static DeadState _instance; //holds one instance of the state


    private DeadState()     
    {   //sets the instance if not already assigned
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static DeadState Instance
    {   //gets the instance, sets it if not already assigned
        get
        {
            if (_instance == null)
            {
                new DeadState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {   //he is dead, goes belly up and stays that way
        own.newState = null;
        own.transform.forward = new Vector3(0, 1, 0);
        if (own.caster.holding) //releases the spell if holding one
            own.caster.Release();
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
    }
}

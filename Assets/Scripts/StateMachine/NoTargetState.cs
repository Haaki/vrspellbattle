﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class NoTargetState : State<ElementalAI>
{
    private static NoTargetState _instance; //holds the one instance of the state

    private NoTargetState() //sets the instance if not set already
    {
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static NoTargetState Instance    //gets the instance or makes a new one if it is not assigned
    {
        get
        {
            if (_instance == null)
            {
                new NoTargetState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {   //no targets so time to spin
        own.transform.Rotate(new Vector3(0, 1, 0), 10);
    }
}

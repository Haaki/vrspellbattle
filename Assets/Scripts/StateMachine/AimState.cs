﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class AimState : State<ElementalAI>
{
    private static AimState _instance;  //holds one instance of the state

    private AimState()
    {   //sets the instance if not already set
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static AimState Instance
    {   //gets the instance, sets it if not already assigned
        get
        {
            if (_instance == null)
            {
                new AimState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
        if (own.newState != null)
        {
            own.stateMachine.ChangeState(own.newState);
        }
        else
        {   
            Vector3 aimVector = new Vector3(0,0,0); //the vector containing information about targeting the spell
            own.transform.forward = Vector3.Normalize(own.enemies[own.target].transform.position - own.transform.position); //looks at target

            if (own.caster.currentSpell.targeting == Spell.Targeting.Arc)
            {   //if arc spell calculates a crude arc to hit target
                aimVector = own.transform.forward + new Vector3(0, (own.transform.position - own.enemies[own.target].transform.position).magnitude / 50, 0);
            }
            else if (own.caster.currentSpell.targeting == Spell.Targeting.Point || own.caster.currentSpell.targeting == Spell.Targeting.Entity)
            {   //if point or entinty targeting, sets targeting straight forward
                aimVector = own.transform.forward;
            }

            own.caster.aimTransform.forward = aimVector;    //sets aiming

            if (own.enemies[own.target].GetComponent<Damageable>().health > 0)  //if target is alive still
                own.newState = CastSpellState.Instance;     //goes to cast spell state
            else
                own.newState = ChooseTargetState.Instance;  //goes to choose target state
        }
    }
}

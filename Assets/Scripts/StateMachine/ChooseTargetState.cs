﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class ChooseTargetState : State<ElementalAI>
{
    private static ChooseTargetState _instance; //holds one instance of the state


    private ChooseTargetState()
    {   //sets instance if not already set
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static ChooseTargetState Instance
    {   //gets instance, sets it if not already assigned
        get
        {
            if (_instance == null)
            {
                new ChooseTargetState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
        if (own.newState != null)
        {
            own.stateMachine.ChangeState(own.newState);
        }
        else
        {
            for (int i = 0; i < own.enemies.Count; i++)
            {   //goes through every enemy
                if (own.enemies[i].GetComponent<Damageable>().health > 0)
                {   //sets new target if it is alive
                    own.target = i;
                }
            }
            if (own.enemies[own.target].GetComponent<Damageable>().health > 0)
            {   //if set target is alive
                own.newState = AimState.Instance;   //goes to aiming state
            }
            else
            {   //if target is not alive 
                own.newState = NoTargetState.Instance;  //goes to no target state
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using States;

public class ChooseSpellState : State<ElementalAI>
{
    private static ChooseSpellState _instance;  //holds one instance of the state


    private ChooseSpellState()
    {   //sets instance if not already set
        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static ChooseSpellState Instance
    {   //gets the instance of the state, sets it if not already assigned
        get
        {
            if (_instance == null)
            {
                new ChooseSpellState();
            }

            return _instance;
        }
    }

    public override void EnterState(ElementalAI own)
    {
        own.newState = null;
    }

    public override void ExitState(ElementalAI own)
    {
    }

    public override void UpdateState(ElementalAI own)
    {
        if (own.newState != null)
        {
            own.stateMachine.ChangeState(own.newState);
        }
        else
        {
            if (!own.caster.holding)
            {   //if it is not holding a spell
                int spellIndex = Mathf.FloorToInt(Random.value * own.spells.Length);    //chooses a random spell
                own.caster.SetCurrentSpell(own.spells[spellIndex]);
            }

            own.newState = AimState.Instance;   //goes to aiming state
        }
    }
}

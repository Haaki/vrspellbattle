﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Caster : NetworkBehaviour
{
    public Transform aimTransform;

    public int[] loadOut = new int[4];
    public string[] loadOutNames = new string[4];
    public List<GameObject> spellPrefabs;
    public List<GameObject> spells;
    List<GameObject> charges;
    int currentSpellID = 0;
    public bool previous;
    public Spell currentSpell;
    public bool next;

    public bool isTurret;
    bool isAiming = true;
    Transform target;
    float lastCast = 0;

    ParticleSystem aimParticles;
    public ParticleSystem chargeParticles;

    [HideInInspector]
    public bool holding;
    float castTime = 0;
    float range = 20;

    private void Start()
    {
        if (isLocalPlayer) {
            transform.position = GameObject.Find("Spawn 1").transform.position;

            //  Load selected spells
            for (int i = 0; i < 4; i++)
                loadOut[i] = GameObject.Find("Game Manager").GetComponent<GameController>().loadOuts[0, i];

            //  Slot 1, up
            switch (loadOut[0]) {
                case 0: loadOutNames[0] = "Firebolt"; break;  //  Firebolt
                case 1: loadOutNames[0] = "Magic Missile"; break;  //  Magic Missile
                case 2: loadOutNames[0] = "Ice Knife"; break;  //  Ice Knife
                case 3: loadOutNames[0] = "AcidRain"; break;  //  Acid Rain
            }
            //  Slot 2, right
            switch (loadOut[1]) {
                case 0: loadOutNames[1] = "Chain Lightning"; break;  //  Chain Lightning
                case 1: loadOutNames[1] = "Fireball"; break;  //  Fireball
                case 2: loadOutNames[1] = "Colour Spray"; break;  //  Colour Spray
                case 3: loadOutNames[1] = "Prismatic Spray"; break;  //  Prismatic Spray
            }
            //  Slot 3, left
            switch (loadOut[2]) {
                case 0: loadOutNames[2] = "Wall of Ice"; break;  //  Wall of Ice
                case 1: loadOutNames[2] = "Wall of Stone"; break;  //  Wall of Stone
            }
            //  Slot 4, down
            switch (loadOut[3]) {
                case 0: loadOutNames[3] = "Magic Missile"; break;  //  Arcane
                case 1: loadOutNames[3] = "Magic Missile"; break;  //  Fire
                case 2: loadOutNames[3] = "Magic Missile"; break;  //  Necrotic
                case 3: loadOutNames[3] = "Magic Missile"; break;  //  Ice
            }
        }

        if (aimTransform == null)
        {
            Debug.LogWarning("No aimTransform set!");
            aimTransform = transform;
        }
        if (isLocalPlayer)
            aimParticles = Instantiate(Resources.Load<GameObject>("Prefabs/ParticleEffects/AimParticles"), aimTransform).GetComponent<ParticleSystem>();

        CmdSpawnChargeParticles();
        spellPrefabs = FindObjectOfType<NetworkManager>().spawnPrefabs.FindAll(a => a.GetComponent<Spell>());
        spells = new List<GameObject>();
        foreach (GameObject prefab in spellPrefabs)
        {
            GameObject sp = Instantiate(prefab, aimTransform);
            sp.name = prefab.name;
            sp.GetComponent<Spell>().caster = gameObject;
            spells.Add(sp);
        }
        foreach (GameObject spell in spellPrefabs)
        {
            if (spell.GetComponent<Spell>().modifiers.Contains(Spell.Modifier.Overcharge))
            {
                Spell sp = spells.Find(a => a.name.Contains(spell.name)).GetComponent<Spell>();
                sp.overchargedSpell = spellPrefabs.Find(a => a.name.Contains(sp.overchargedSpell.name));
                spells.Remove(spells.Find(a => a.name.Contains(sp.overchargedSpell.name)));
            }
        }
        currentSpell = spells[currentSpellID].GetComponent<Spell>();
        currentSpell.gameObject.SetActive(true);
        chargeParticles = GetChargeEffect(currentSpell);

        SetCurrentSpell(loadOutNames[0]);
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            if (Input.GetButtonDown("Submit")) {

                //  Right
                if (Input.GetAxis("ScrollX") > .3f && Mathf.Abs(Input.GetAxis("ScrollY")) < .4f)
                    SetCurrentSpell(loadOutNames[1]);

                //  Left
                if (Input.GetAxis("ScrollX") < -.3f && Mathf.Abs(Input.GetAxis("ScrollY")) < .4f)
                    SetCurrentSpell(loadOutNames[2]);

                //  Up
                if (Input.GetAxis("ScrollY") > .3f && Mathf.Abs(Input.GetAxis("ScrollX")) < .4f)
                    SetCurrentSpell(loadOutNames[0]);

                //  Down
                if (Input.GetAxis("ScrollY") < -.3f && Mathf.Abs(Input.GetAxis("ScrollX")) < .4f)
                    SetCurrentSpell(loadOutNames[3]);

                Debug.Log(Input.GetAxisRaw("ScrollX") + ", " + Input.GetAxisRaw("ScrollY"));
            }

                            if (Input.GetKeyDown(KeyCode.B))
                previous = true;
            if (Input.GetKeyDown(KeyCode.N))
                next = true;
            if (Input.GetKeyDown(KeyCode.F))
                SetCurrentSpell("Fireball");
            if (Input.GetKeyDown(KeyCode.M))
                SetCurrentSpell("Magic Missile");
            if (Input.GetKeyDown(KeyCode.P))
                SetCurrentSpell("Prismatic Spray");
            if (Input.GetKeyDown(KeyCode.L))
                SetCurrentSpell("Lightning Bolt");

            
            if (Input.GetAxis("Fire1") > 0.02f && !holding)
                Hold();
            if (Input.GetAxis("Fire1") <= 0.02f && holding)
                Release();


            //Vector3 end = aimTransform.position + aimTransform.forward * range;
            int layerMask = 1 << LayerMask.NameToLayer("Player");
            layerMask = ~layerMask;
            RaycastHit rayHit;
            if (Physics.Raycast(new Ray(aimTransform.position, aimTransform.forward), out rayHit, range, layerMask)) {
                target = rayHit.transform;
                //end = rayHit.point;

                //Teleport to target
                if (Input.GetButtonDown("Fire2"))
                    aimParticles.Play();
                if (Input.GetButtonUp("Fire2")) {
                    if (Input.GetAxis("Fire1") <= 0.02f) {
                        aimParticles.Stop();
                        aimParticles.Clear();
                    }
                    Teleport(rayHit.point);
                }
                
            }
        }
        if (isTurret)
        {
            if (currentSpell.castingType == Spell.CastingType.Charge)
            {
                float holdtime = 1.1f * currentSpell.castingTime;

                if (currentSpell.modifiers.Contains(Spell.Modifier.Overcharge))
                {
                    holdtime = currentSpell.overchargedSpell.GetComponent<Spell>().castingTime * 1.1f;
                    if (currentSpell.overchargedSpell.GetComponent<Spell>().modifiers.Contains(Spell.Modifier.Multiple))
                        holdtime *= currentSpell.overchargedSpell.GetComponent<Spell>().multiAmount;
                }
                else if (currentSpell.modifiers.Contains(Spell.Modifier.Multiple))
                    holdtime *= currentSpell.multiAmount;


                    if (!holding && Time.time - lastCast > 0.5f)
                {
                    lastCast = Time.time;
                    Hold();
                }
                else if (Time.time - lastCast > holdtime)
                {
                    Release();
                    lastCast = Time.time;
                }
            }
            else if (!holding)  //  Channel spell
            {
                Hold();
                lastCast = Time.time;
            }
        }

        if (holding)
        {
            if (currentSpell.castingType == Spell.CastingType.Channel)
                if (Time.time - castTime > currentSpell.castingTime)
                {
                    if (currentSpell.targeting != Spell.Targeting.Entity || currentSpell.target != null)
                        currentSpell.Cast();
                    castTime = Time.time;
                }
        }

        if (currentSpell.targeting == Spell.Targeting.Entity && isAiming)
            EntityAim();

        if (previous || next)
        {
            Release();
            currentSpellID = (next) ? Mathf.Min(currentSpellID + 1, spells.Count - 1) : Mathf.Max(currentSpellID - 1, 0);
            currentSpell = spells[currentSpellID].GetComponent<Spell>();
            previous = next = false;
            if (isLocalPlayer)
                UpdateAimEffect();
            chargeParticles = GetChargeEffect(currentSpell);
        }
    }

    public void SetCurrentSpell(string spellName)
    {
        if(holding)
            Release();
        GameObject setSpell = spells.Find(a => a.name.Contains(spellName));
        currentSpellID = spells.IndexOf(setSpell);
        currentSpell = setSpell.GetComponent<Spell>();
        chargeParticles = GetChargeEffect(currentSpell);
    }

    public void Hold()
    {
        holding = true;
        isAiming = (currentSpell.castingType == Spell.CastingType.Charge);
        castTime = Time.time;
        if(isLocalPlayer)
            UpdateAimEffect();
        chargeParticles?.Play();
    }

    public void Release()
    {
        holding = false;
        isAiming = !(currentSpell.castingType == Spell.CastingType.Charge);
        if (currentSpell.castingType == Spell.CastingType.Charge)
            if (Time.time - castTime > currentSpell.castingTime)
            {
                //Debug.Log($"casting {currentSpell.name} after {Time.time - castTime} seconds");
                if (currentSpell.modifiers.Contains(Spell.Modifier.Overcharge) && Time.time - castTime > currentSpell.overchargedSpell.GetComponent<Spell>().castingTime)
                {
                    ReleaseSpell(currentSpell.overchargedSpell.GetComponent<Spell>());
                }
                else
                    ReleaseSpell(currentSpell);
            }
        if(isLocalPlayer)
            UpdateAimEffect();

        chargeParticles?.Stop();
        chargeParticles?.Clear();
    }

    void ReleaseSpell(Spell spell)
    {
        if (spell.modifiers.Contains(Spell.Modifier.Multiple))
        {
            int i = 1;
            while (Time.time - castTime > spell.castingTime * i)
                i++;
            spell.multiCharge = Mathf.Min(i, spell.multiAmount);
        }
        if (spell.targeting != Spell.Targeting.Entity || spell.target != null)
            spell.Cast();
    }

    void EntityAim()
    {
        RaycastHit rayHit;

        if (Physics.Raycast(new Ray(transform.position, transform.forward), out rayHit, currentSpell.pointRange))
            if (rayHit.transform.GetComponent<Damageable>())
                target = currentSpell.target = rayHit.transform;

        if (isLocalPlayer)
            UpdateAimEffect();
    }

    ParticleSystem GetChargeEffect(Spell spell)
    {
        //Debug.Log($"Attemting to find {spell.element}Charge");
        Transform cp = aimTransform.Find($"{spell.element}Charge");
        return cp?.GetComponent<ParticleSystem>();
    }


    void UpdateAimEffect()
    {
        if (currentSpell.targeting == Spell.Targeting.Self || !isAiming)
        {
            aimParticles.Stop();
            aimParticles.Clear();
        }
        else
        {
            aimParticles.transform.Find("Twinkle").gameObject.SetActive(currentSpell.targeting != Spell.Targeting.Entity);  //  Turn off Beam if entity target

            var velOverLife = aimParticles.velocityOverLifetime;                                //  Set values for point / arc targeting
            velOverLife.z = currentSpell.arcSpeed;
            var apMain = aimParticles.main;
            apMain.gravityModifier = (currentSpell.targeting == Spell.Targeting.Arc) ? 1 : 0;

            aimParticles.Play();
        }
    }
    [Command]
    public void CmdSpawnChargeParticles()
    {
        List<GameObject> chPrefabs = FindObjectOfType<NetworkManager>().spawnPrefabs.FindAll(a => a.name.Contains("Charge"));
        foreach (GameObject chPrefab in chPrefabs)
        {
            GameObject charge = Instantiate(chPrefab, aimTransform);
            NetworkServer.Spawn(charge);
            charge.name = chPrefab.name;
            RpcUpdateChargeParticle(charge);
        }
    }
    [ClientRpc]
    public void RpcUpdateChargeParticle(GameObject charge)
    {
        if (charges != null)
            charges.Add(charge);
    }
    [Command]
    public void CmdSpawnSpell(string spellName, GameObject caster)
    {
        GameObject spell = Instantiate(FindObjectOfType<NetworkManager>().spawnPrefabs.Find(a => spellName.Contains(a.name)), caster.GetComponent<Caster>().aimTransform.position, caster.GetComponent<Caster>().aimTransform.rotation);
        NetworkServer.Spawn(spell);
        RpcUpdateSpell(spell, caster);
    }
    [ClientRpc]
    public void RpcUpdateSpell(GameObject spellPrefab, GameObject caster) {
        Spell spell = spellPrefab.GetComponent<Spell>();
        spell.caster = caster;
        NetworkTransform nt = spellPrefab.AddComponent<NetworkTransform>();
        nt.transformSyncMode = NetworkTransform.TransformSyncMode.SyncRigidbody3D;
        nt.sendInterval = 0;
        Destroy(spellPrefab, spell.decay);
        spell.target = caster.GetComponent<Caster>().target;

        if (spell.spellType == Spell.SpellType.Projectile) {
            spellPrefab.transform.position = aimTransform.position;
            Projectile p = spellPrefab.AddComponent<Projectile>();
            p.launchTime = Time.time;
            p.launched = true;
            Rigidbody rb = spellPrefab.AddComponent<Rigidbody>();
            rb.useGravity = (spell.targeting == Spell.Targeting.Arc);
            rb.velocity = aimTransform.forward * spell.projectileSpeed + new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f) * spell.spread;
            if (spell.modifiers.Contains(Spell.Modifier.Homing))
                spellPrefab.AddComponent<Homing>();
            SphereCollider sc = spellPrefab.AddComponent<SphereCollider>();
            sc.radius = spell.projectileSize / 2;
            sc.isTrigger = true;
        }
        else if (spell.spellType == Spell.SpellType.Area || spell.spellType == Spell.SpellType.Minion) {
            if (spell.spellType == Spell.SpellType.Minion) Debug.Log("Banana!");
            int layerMask = 1 << LayerMask.NameToLayer("Player");
            layerMask = ~layerMask;
            RaycastHit rayHit;
            if (Physics.Raycast(new Ray(aimTransform.position, aimTransform.forward), out rayHit, currentSpell.pointRange, layerMask))
                if (!spell.grounded || rayHit.transform.tag == "Ground") {
                    spellPrefab.transform.position = rayHit.point;
                    Area a = spellPrefab.AddComponent<Area>();
                    a.spell = spell;
                    a.lastLaunch = Time.time;
                    a.launched = true;
                }
        }
    }
    [Command]
    public void CmdSummonCreature(GameObject spell)
    {
        GameObject creature = Instantiate(FindObjectOfType<NetworkManager>().spawnPrefabs.Find(a => a.name.Contains(spell.GetComponent<Spell>().minionPrefab.name)), spell.transform.position, spell.transform.rotation);
        NetworkServer.Spawn(creature);
        RpcUpdateCreature(creature, spell);
    }
    [ClientRpc]
    public void RpcUpdateCreature(GameObject creature, GameObject spell)
    {
        ElementalAI eai = creature.GetComponent<ElementalAI>();
        eai.caster = spell.GetComponent<Spell>().caster.GetComponent<Caster>();
        List<GameObject> casters = new List<GameObject>();
        casters.AddRange(FindObjectsOfType<GameObject>());
        eai.enemies = casters.FindAll(a => a.GetComponent<Caster>() && a != spell.GetComponent<Spell>().caster && a != creature);
    }

    void Teleport(Vector3 pos) {
        //Debug.Log("Teleport");
        if (target.gameObject.tag == "Ground") {
            //Debug.Log("Teleport Hit Ground");
            transform.position = pos;
        }
    }
}

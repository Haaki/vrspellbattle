﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Damageable : MonoBehaviour
{
    public float health;
    public int invulFrames = 1;
    public int invul = 1;
    public List<Spell.Element> vulnerabilities;
    public List<Spell.Element> resistances;
    public List<Spell.Element> immunities;


    private void Update()
    {
        if (invul > 0) invul--;
    }
    public bool DealDamage(float amount, Spell.Element element)
    {
        if (invul <= 0)
        {
            float adjustedDamage = amount;
            if (immunities.Contains(element))   //  Immune
                return false;
            else if (health <= 0)   //  Already dead
                return false;
            if (resistances.Contains(element))
                adjustedDamage *= 0.5f;
            if (vulnerabilities.Contains(element))
                adjustedDamage *= 2;

            //Debug.Log($"{name} took {adjustedDamage} damage");
            health = health - adjustedDamage;
            invul += invulFrames;

            if (gameObject.tag == "Player" && health <= 0)
                GameObject.Find("Network Manager").GetComponent<NetworkManager>().StopHost();

            return true;
        }
        else
            return false;
    }

}

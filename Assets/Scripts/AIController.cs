﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    float walkCool = 1;             //cooldown for walking
    Vector3 aimVector = new Vector3(0, 0, 0);   //pointing in the direction of aiming
    Vector3 walkVector;             //pointing in the direction of walking
    Vector3 aimPoint;               //a point aimed at
    int target = 0;                 //index of current target
    float[] aggro;                  //a list of aggro towards different enemies
    Caster caster;                  //caster component, needed for casting spells
    float holdTimer = -1;           //a timer for holding a spell
    float viewAngle = 45;           //angle of view for AI
    float viewDistance = 45;        //how far the AI can see
    float turnRate = 5;             //how fast the AI can turn, not really in use 
    float aggroThreshhold = 1;      //difference in aggro needed for AI to change target
    bool[] seen;                    //a list bools for having seen an enemy or not
    float[] distance;               //a list of distances to targets
    bool moveNeeded = false;        //true if a move is needed
    Vector3 moveDirection;          //direction needed for moving when a move is needed

    public List<GameObject> enemies;        //list of enemies
    public float speed;                 //speed of AI
    public float interestLoss = 0.2f;   //how fast the AI loses interest
    public float interestRate = 0.2f;   //how fast the AI gains interest in an enemy
    public bool strongesFirst = false;  //if the AI targets strongest or weakest first
    public float personalSpace = 1.0f;  //how close the AI will tolerate an enemy to get before moving away

    // Use this for initialization
    void Start()    //resets aggro, seen array, and distance array and inits caster component
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
            enemies.Add(players[i]);

        GameObject[] AIs = GameObject.FindGameObjectsWithTag("AI");
        for (int i = 0; i < AIs.Length; i++)
            if (AIs[i] != gameObject)
                enemies.Add(AIs[i]);

        aggro = new float[enemies.Count];
        seen = new bool[enemies.Count];
        distance = new float[enemies.Count];
        for(int i = 0; i < enemies.Count; i++)
        {
            aggro[i] = 0.0f;
            seen[i] = false;
            distance[i] = 100000f;
        }
        caster = GetComponent<Caster>();
        caster.aimTransform = transform.Find("right hand").Find("Wand").Find("Point");
    }

    private void Update()
    {
        walkCool -= Time.deltaTime;     //update timers and cooldowns
        holdTimer -= Time.deltaTime;

        if (GetComponent<Damageable>().health > 0)  //if AI is alive
        {
            Look();             //looks if it can see any enemies
            UpdateTargets();    //updates targets
            if (target != -1)   //if a target was found
            {
                if (caster.currentSpell.castingType == Spell.CastingType.Channel && Random.value < 0.05f)   //if holding a channel spell, small chance of releasing early
                    holdTimer = -1; //will release spell
                if(!caster.holding) //if not already holding a spell
                    chooseSpell();  //chooses a new spell
                Aim();          //aims current spell
                CastSpell();    //casts spell
                if (walkCool < 0.0f)    //if its long enough since it has moved
                {  
                    Move();         //moves and resets walk cooldown
                    walkCool = 0.5f;
                }
            }
        }
        else    //if AI is dead
        {       //lays on its back and releases the spell its holding if it is holding one
            transform.forward = new Vector3(0, 1, 0);
            if(caster.holding)
                caster.Release();
        }
    }

    private void Look()     //looks for enemies
    {
        bool enemySeen = false;     //if it has seen one enemy
        for (int i = 0; i < seen.Length; i++)   //resets seen array
            seen[i] = false;
        foreach (Collider hit in Physics.OverlapSphere(transform.position, viewDistance))
        {   //goes through every hitbox within the viewDistance
            Vector3 hitDir = hit.transform.position - transform.position;   //the direction to the hit from the AI
            if (Vector3.Angle(hitDir, transform.forward) < viewAngle)       //checks if the angle is within the field of view
            {
                for(int i = 0; i < enemies.Count; i++)
                {   //goes through every enemy
                    if (enemies[i] != null)
                        if (hit.name == enemies[i].name) {   //checks is the hit is an enemy
                            enemySeen = true;   //has seen one enemy
                            seen[i] = true;     //has seen that enemy
                            distance[i] = Vector3.Magnitude(hit.transform.position - transform.position);   //sets distance to seen target
                            if (distance[i] < personalSpace) //if the target is too close
                            {
                                moveNeeded = true;  //needs to move away
                                moveDirection = Vector3.Normalize(-(hit.transform.position - transform.position));
                            }
                        }
                }
            }
        }

        if(!enemySeen)
        {   //if no enemy seen, just turn around
            transform.Rotate(transform.up, turnRate);
        }
    }

    private void UpdateTargets()    //updates all targets
    {
        bool liveTargets = false;   //true if there are enemies alive
        for(int i = 0; i < enemies.Count; i++)
        {   //goes through every enemy
            if (enemies[i] != null)
                if (enemies[i].GetComponent<Damageable>()?.health > 0) {   //if enemy is alive
                    liveTargets = true; //has at least one live enemy
                    if (aggro[i] > 0.0f) {   //if aggro is positive
                        aggro[i] -= interestLoss;   //loses some interest
                        if (!seen[i])               //if enemy was not seen
                            aggro[i] -= interestLoss * 2;   //loses a lot interest
                    }

                    if (seen[i])    //if enemy was seen
                    {   //gains a lot interest, more if enemy is closer
                        aggro[i] += 2 * interestRate;
                        aggro[i] += (10 / distance[i]) * interestRate;
                        if (strongesFirst) {   //gets more aggro based on health 
                            aggro[i] += enemies[i].GetComponent<Damageable>().health / 100 * interestRate;
                        }
                        else {   //gets less aggro based on health
                            aggro[i] += -enemies[i].GetComponent<Damageable>().health / 100 * interestRate;
                        }
                    }
                }
                else {   //no interest in a dead enemy

                    aggro[i] = -100000;
                }
        }

        for(int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null)
                if (enemies[i].GetComponent<Damageable>().health > 0) {
                    if (aggro[i] > aggro[target] + aggroThreshhold) {   //sets target to enemy with highest aggro (with a threshhold)
                        target = i;
                    }
                }
        }

        if (!liveTargets)   //if no live enemies
            target = -1;    //has no target
    }

    private void chooseSpell()  //chooses a new spell
    {
        if(Random.value < 0.2f) //20% chance to choose new spell
        {   
            float newSpell = Random.value;  //chooses a random spell
            if(newSpell < 0.1f) //10% chance for fireball
            {
                caster.SetCurrentSpell("Fireball");
            }
            else if(newSpell < 0.2f)    //10% chance for ligtnin bolt
            {
                caster.SetCurrentSpell("Lightning Bolt");
            }
            else if(newSpell < 0.3f)    //10% chance for prismatic spray
            {
                caster.SetCurrentSpell("Prismatic Spray");
            }
            else if(newSpell < 0.9f)    //60% chance for ice knife
            {
                caster.SetCurrentSpell("Ice Knife");
            }
            else    //10% chance for magic missile
            {
                caster.SetCurrentSpell("Magic Missile");
            }
        }
    }

    private void Aim()  //aims spell
    {
        if (enemies[target] == null)
            return;

        transform.forward = Vector3.Normalize(enemies[target].transform.position - transform.position); //looks towards target

        if(caster.currentSpell.targeting == Spell.Targeting.Arc)
        {   //if spell is arcing
            aimVector = transform.forward + new Vector3(0, (transform.position - enemies[target].transform.position).magnitude / 50, 0);
        }
        else if(caster.currentSpell.targeting == Spell.Targeting.Point || caster.currentSpell.targeting == Spell.Targeting.Entity)
        {   //if spell is point or entity
            aimVector = transform.forward;  //aims at target
        }

        caster.aimTransform.forward = aimVector;    //sets spells aim 
    }

    public void Move()  //moves AI
    {
        if (moveNeeded) //if a specific move is needed
        {
            moveNeeded = false;     //does not need specific move anymore
            walkVector = moveDirection; //sets direction 
        }
        else    
        {   //moves randomly left or right at random speed
            walkVector = (Random.value > 0.5f) ? transform.right : -transform.right;
            walkVector += new Vector3((Random.value - 0.5f) * 2, 0, (Random.value - 0.5f) * 2);
            walkVector = Vector3.Normalize(walkVector);
        }   //sets velocity
        GetComponent<Rigidbody>().velocity = walkVector * speed * (Random.value + 0.5f);
    }

    void CastSpell()    //casts spell
    {
        if (caster.currentSpell.castingType == Spell.CastingType.Channel)   //if spell is channeling
        {
            if(!caster.holding) //if not holding a spell
            {
                holdTimer = 100;    //sets a hold timer
                caster.Hold();      //starts holding a spell
            }
        }
        else
        {
            if (!caster.holding)    //if not holding a spell
            {   //sets hold timer to a value depending on spell
                holdTimer = caster.currentSpell.castingTime * ((caster.currentSpell.modifiers.Contains(Spell.Modifier.Multiple)) ? 10 : 1.1f);
                caster.Hold();
            }
        }

        if (holdTimer < 0)
        {   //casts spell
            caster.Release();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trace : MonoBehaviour {

    public float time = 1;
    float timeRatio;
    OldSpell spell;
    float startTime;

    private void Start()
    {
        spell = GetComponent<OldSpell>();
        startTime = Time.time;
    }

    private void FixedUpdate()
    {
        timeRatio = Mathf.Min((Time.time - startTime) / time, 1);
        transform.position = Vector3.Lerp(spell.origin, spell.position, timeRatio);
        if (Time.time > startTime + time * 1.2f)
        {
            Collider c = GetComponent<Collider>();
            if (c) c.isTrigger = true;
            Destroy(this);
        }
    }
}

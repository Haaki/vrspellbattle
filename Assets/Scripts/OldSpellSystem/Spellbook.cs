﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spellbook : MonoBehaviour {

    public Spellhand spellHand;
    public uint spellID = 0;
    public List<GameObject> Spells;
    public List<Texture> SpellIcons;
    public GameObject book;

    void Update()
    {
        if (book)
        {
            if (spellID == spellHand.spellID)
                book.GetComponent<Renderer>().material.color = Color.yellow;
            else
                book.GetComponent<Renderer>().material.color = Color.white;

            book.GetComponent<Renderer>().material.mainTexture = SpellIcons[(int)spellID];
        }

        if (Mathf.Abs(Input.GetAxis("Scroll")) > .25f)
        {
            //Debug.Log(Input.GetAxis("Scroll"));
            if (Input.GetButtonDown("Submit"))
            {
                Debug.Log("Page flip");
                spellID = (uint)Mathf.Clamp( (int)spellID + Mathf.Sign(Input.GetAxis("Scroll")), 0, Spells.Count-1);
            }
        }

        if (Input.GetAxis("Select") == 1)
        {
            Debug.Log($"Spell {spellID} selected");
            spellHand.spellID = spellID;
        }
    }
}

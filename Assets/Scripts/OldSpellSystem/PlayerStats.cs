﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public int playerID = 0;
    public int health = 100;
    public int maxHealth = 100;
    public float mass = 10;
    public float speed = 5;

	// Use this for initialization
	void Start ()
    {
        GetComponent<Rigidbody>().mass = mass;
        if (GetComponent<AIController>())
            GetComponent<AIController>().speed = speed;

        health = maxHealth;
    }
	
	// Update is called once per frame
	void Update ()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
	}

    void OnGUI()
    {
        float sw = Screen.width;
        float sh = Screen.height;
        float s = sh / 1080;
        int p = ((playerID == 0) ? 1 : -1);
        GUI.Box(new Rect(sw/2 + s*p*500 - s*100, s*200, s*200, s*50), "");
        GUI.Box(new Rect(sw/2 + s*p*500 - s*100, s*200, s*health*2, s*50), health.ToString());
    }
}

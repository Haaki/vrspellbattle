﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public GameObject cube;
    public GameObject book;

    GameObject[] hpBar = new GameObject[2];
    GameObject[] hpBarBG = new GameObject[2];
    Vector3 pos = new Vector3(-.1f, 0.4f, 0.25f);
    Vector3 scale = new Vector3(1f, 0.1f, 0.4f);

    public PlayerStats[] stats;

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < 2; i++)
        {
            hpBar[i] = Instantiate<GameObject>(cube);
            hpBar[i].transform.parent = book.transform;
            hpBar[i].transform.localPosition = new Vector3(pos.x, pos.y, pos.z * ((i == 0) ? 1 : -1));
            hpBar[i].transform.localEulerAngles = Vector3.zero;
            hpBar[i].transform.localScale = scale;
            hpBar[i].GetComponent<Renderer>().material.color = ((i == 0) ? Color.green : Color.red);
            hpBarBG[i] = Instantiate<GameObject>(cube);
            hpBarBG[i].transform.parent = book.transform;
            hpBarBG[i].transform.localEulerAngles = Vector3.zero;
            hpBarBG[i].transform.localScale = hpBar[i].transform.localScale * 1.01f;
            hpBarBG[i].transform.localPosition = hpBar[i].transform.localPosition - new Vector3(hpBar[i].transform.localPosition.x / 2f, 0, 0);

            hpBarBG[i].GetComponent<Renderer>().material.color = Color.grey;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < 2; i++)
        {
            hpBar[i].transform.localPosition = new Vector3(pos.x, pos.y, (pos.z + (pos.z * (1 - (float)(stats[i].health) / stats[i].maxHealth))/1.25f) * ((i == 0) ? 1 : -1));
            hpBar[i].transform.localScale = new Vector3(scale.x, scale.y, scale.z * ((float)(stats[i].health) / stats[i].maxHealth));
        }
    }
}

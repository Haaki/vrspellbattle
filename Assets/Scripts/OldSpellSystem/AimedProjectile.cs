﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimedProjectile : MonoBehaviour {

    public float speed = 10;
    public float decay = 10;

    void Start ()
    {
        OldSpell spell = GetComponent<OldSpell>();
        transform.forward = spell.aim;
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb == null) rb = gameObject.AddComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
        Destroy(gameObject, decay);
    }
}

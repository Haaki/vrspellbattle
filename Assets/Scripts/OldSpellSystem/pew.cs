﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pew : MonoBehaviour {

    private SteamVR_TrackedController controller;

    public GameObject projectilePrefab;
    public float speed;
    public float lifetime;
    bool triggered = false;

    private void FixedUpdate()
    {
        if (Input.GetAxis("Fire1") == 1 && !triggered)
        {
            Debug.Log($"pew{Input.GetAxis("Fire1")}");
            triggered = true;
            GameObject projectile = Instantiate(projectilePrefab);
            projectile.name = projectilePrefab.name;

            projectile.transform.position = transform.position + transform.forward * .5f;

            projectile.GetComponent<Rigidbody>().velocity = GetComponent<Spellhand>().transform.forward * speed;

            Destroy(projectile, lifetime);
        }
        else if (Input.GetAxis("Fire1") != 1 && triggered)
        {
            Debug.Log("no pew");
            triggered = false;
        }
    }
}

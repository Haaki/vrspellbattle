﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class simpleMovement : NetworkBehaviour {

    GameObject spellPrefab;

    Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        spellPrefab = Resources.Load<GameObject>("Prefabs/Spells/Firebolt");
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<Renderer>().material.color = Color.blue;
    }

    void FixedUpdate ()
    {
        if (isLocalPlayer)
        {
		    if (Input.GetAxisRaw("Vertical") != 0)
                rb.velocity = transform.forward * Input.GetAxis("Vertical");
            if (Input.GetAxisRaw("Horizontal") != 0)
                transform.Rotate(new Vector3(0, 1, 0), Input.GetAxisRaw("Horizontal"));
        }
	}

    private void Update()
    {
        if (isLocalPlayer)
        {
            //if (Input.GetButtonDown("Fire1"))CmdCastSpell();
        }
    }

    [Command]
    void CmdCastSpell()
    {
        GameObject go = Instantiate(spellPrefab);
        go.transform.position = transform.position + transform.forward;

        OldSpell spell = go.GetComponent<OldSpell>();
        if (spell == default(OldSpell)) go.AddComponent<OldSpell>();

        spell.Cast(gameObject, transform, transform.forward, transform.position);
        NetworkServer.Spawn(go);
    }
}

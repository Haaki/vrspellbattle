﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spellhand : MonoBehaviour {

    public Spellbook spellBook;
    float range = 20;
    public LineRenderer lr;
    public GameObject target;
    public uint spellID = 0;

    Color rayColor = new Color(1, 0, 0, 0.5f);
    Color rayOoRColor = new Color(0.8f, 0.4f, 0.4f, 0.5f);

    Transform spellTarget;
    RaycastHit rayHit;

    bool triggered = false;

    // Use this for initialization
    void Start ()
    {
        target = Instantiate(target);
        target.GetComponent<Renderer>().material.color = rayColor;
        if (lr == null)
        {
            lr = gameObject.AddComponent<LineRenderer>();
            lr.material = Resources.Load<Material>("Materials/Line"); 
        }
        lr.startWidth = lr.endWidth = 0.02f;
        lr.startColor = lr.endColor = rayColor;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 end = transform.position + transform.forward * range;
        int layerMask = 1 << LayerMask.NameToLayer("Player");
        layerMask = ~layerMask;
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out rayHit, range, layerMask))
        {
            spellTarget = rayHit.transform;
            end = rayHit.point;
            target.SetActive(true);
            target.transform.position = end;
            lr.startColor = lr.endColor = rayColor;

            //Teleport to target
            if (Input.GetButtonDown("Fire2"))
                Teleport();
        }
        else
        {
            target.SetActive(false);
            lr.startColor = lr.endColor = rayOoRColor;
        }
            
        //Set line positions
        lr.SetPositions(new Vector3[] {transform.position, end} );

        if (Input.GetAxis("Fire1") == 1 && !triggered)
        {
            Debug.Log($"pew{Input.GetAxis("Fire1")}");
            triggered = true;
            if (spellID != 1 || spellTarget.gameObject.tag == "Ground")
                CastSpell();
        }
        else if (Input.GetAxis("Fire1") != 1 && triggered)
        {
            Debug.Log("no pew");
            triggered = false;
        }
    }

    void Teleport()
    {
        //Debug.Log("Teleport");
        if (spellTarget.gameObject.tag == "Ground")
        {
            //Debug.Log("Teleport Hit Ground");
            transform.parent.position = new Vector3(target.transform.position.x, transform.parent.position.y, target.transform.position.z); 
        }
    }

    void CastSpell()
    {
        GameObject go = Instantiate(spellBook.Spells[(int)spellID]);
        go.transform.position = transform.position;
        OldSpell spell = go.GetComponent<OldSpell>();
        if (spell == default(OldSpell))
            go.AddComponent<OldSpell>();

        spell.Cast(gameObject, spellTarget, transform.forward, rayHit.point);
    }
}

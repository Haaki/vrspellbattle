﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class OldSpell : MonoBehaviour {

    public GameObject caster;
    public Transform target;
    public Vector3 aim;
    public Vector3 position;

    public Vector3 origin;
    
	void Start () 
    {
        if (origin == default(Vector3))
            origin = transform.position;
	}

    public void Cast(GameObject c, Transform t, Vector3 a, Vector3 p)
    {
        caster = c;
        target = t;
        aim = a;
        position = p;
    }
}

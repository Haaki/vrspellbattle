﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOrigin : MonoBehaviour {

    public bool caster;
    public Vector3 offset;
    

    private void Start()
    {
        OldSpell spell = GetComponent<OldSpell>();
        spell.origin = (caster) ? spell.caster.transform.position + offset : spell. position + offset;
        transform.Find("Lightning").GetComponent<ParticleSystem>().Play();
    }
}

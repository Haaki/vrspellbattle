﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Split : MonoBehaviour {

    public GameObject prefab;
    public float radius = 10;
    public int amount;
    public int recursive;

    private void OnTriggerEnter(Collider other)
    {
        List<Collider> hits = new List<Collider>(Physics.OverlapSphere(transform.position, radius));
        int i = 0;
        if (recursive >= 0)
        {
            hits = hits.OrderBy(a => Random.value).ToList();
            foreach (Collider hit in hits)
            {
                if (hit.tag != "Ground")
                    if (i < amount )
                    {
                        GameObject go = Instantiate(prefab);
                        go.name = name;
                        go.transform.position = transform.position;
                        go.GetComponent<Collider>().isTrigger = false;
                        go.GetComponent<Split>().recursive = recursive - 1;
                        go.GetComponent<Trace>().enabled = true;

                        OldSpell spell = go.GetComponent<OldSpell>();
                        spell.Cast(gameObject, hit.transform, transform.forward, hit.transform.position);
                        spell.origin = transform.position;
                        i++;
                    }
            }
        }
    }
}

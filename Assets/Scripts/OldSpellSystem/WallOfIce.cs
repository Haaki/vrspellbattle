﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallOfIce : MonoBehaviour {

    float range = 20;

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit rayHit;
        int layerMask = 1 << LayerMask.NameToLayer("Player");
        layerMask = ~layerMask;
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out rayHit, range, layerMask))
        {
            //Spawn Wall
            if (Input.GetAxis("Fire1") == 1)
            {
                if (rayHit.transform.gameObject.tag == "Ground")
                {
                    GameObject wall = new GameObject("WallOfIce");
                    wall.transform.localScale = Vector3.one * 1f;
                    wall.transform.position = rayHit.point + Vector3.up * 0.6f * wall.transform.localScale.y;
                    wall.AddComponent<WallOrganizer>();
                    wall.transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnImpact : MonoBehaviour {

    public List<ParticleSystem> effects;
    public float delay = 5.0f;
    public float iceBreakRadius = 3;
    public float forceRadius = 3;
    public float forceMagnitude = 10;

    Rigidbody rb;
    bool done = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!done && other.GetComponent<PlayerStats>())
            other.GetComponent<PlayerStats>().health -= 5;

        if (!done && other.transform.parent && other.transform.parent.GetComponent<PlayerStats>())
            other.transform.parent.GetComponent<PlayerStats>().health -= 5;

        //Debug.Log($"Hit {other.name}!");
        if (!other.GetComponent<Homing>())
            if (!done && other.name != "[CameraRig]")
                Explode();
    }

    void Explode()
    {
        rb = GetComponent<Rigidbody>();
        if (rb) Destroy(rb);
        Homing homing = GetComponent<Homing>();
        if (homing) Destroy(homing);
        Destroy(GetComponent<Collider>());


        foreach (Transform child in transform)
            child.gameObject.GetComponent<ParticleSystem>().Stop();

        foreach (ParticleSystem effect in effects)
        {
            effect.gameObject.SetActive(true);
            effect.Play();
        }
        done = true;
        if (iceBreakRadius > 0) BreakIce();
        if (forceRadius > 0) AddForce();
        Destroy(this.gameObject, delay);
    }

    void BreakIce()
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, iceBreakRadius);
        foreach (Collider hit in hits)
            if (hit.tag == "Ice")
                Destroy(hit.gameObject);
    }

    void AddForce()
    {
        Collider[] hits = Physics.OverlapSphere(transform.position, forceRadius);
        foreach (Collider hit in hits)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                Vector3 forceVector = hit.transform.position - transform.position;
                if (hit.name.Equals("[CameraRig]"))
                    forceVector = hit.transform.Find("Camera (eye)").position - transform.position;
                rb?.AddForce( forceVector * Mathf.Abs(forceMagnitude * (1 - forceVector.magnitude / forceRadius)), ForceMode.Impulse);
                
                //Debug.Log($"Added {forceMagnitude * (1 - forceVector.magnitude / forceRadius)} force to {hit.name} it was {forceVector} away from explotion");
            }
        }
    }
}

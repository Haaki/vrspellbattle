﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallOrganizer : MonoBehaviour {

    GameObject mid;
    GameObject top;
    GameObject bottom;
    public GameObject[,,] walls;
    Spell spell;

	// Use this for initialization
	void Start ()
    {
        spell = GetComponent<Spell>();

        //  Get wall elements
        if (spell.element == Spell.Element.Ice) {
            mid = Resources.Load<GameObject>("Prefabs/Ice");
            top = Resources.Load<GameObject>("Prefabs/Snowcap");
            bottom = Resources.Load<GameObject>("Prefabs/IceBottom");
        }
        else if (spell.element == Spell.Element.Earth) {
            top = mid = Resources.Load<GameObject>("Prefabs/Rock");
            bottom = Resources.Load<GameObject>("Prefabs/RockBottom");
        }

        //  Set transform
        transform.localScale = Vector3.one * 1f;
        Vector3 pos = transform.position;
        transform.position = pos + Vector3.up * 0.4f * transform.localScale.y;

        //  Create wall
        for (int i = 0; i < walls.GetLength(0); i++)
            for (int j = 0; j < walls.GetLength(1); j++)
                for (int k = 0; k < walls.GetLength(2); k++) {
                    walls[i, j, k] = (j == walls.GetLength(1) - 1) ? Instantiate(top) : (j == 0) ? Instantiate(bottom) : Instantiate(mid);
                    walls[i, j, k].transform.parent = transform;
                    walls[i, j, k].transform.localScale = Vector3.one;
                    walls[i, j, k].transform.localEulerAngles = Vector3.zero;
                    walls[i, j, k].transform.localPosition = new Vector3(i - walls.GetLength(0) / 2, j - walls.GetLength(1), k - walls.GetLength(2) / 2);
                }

        //  Remove spell component
        Destroy(GetComponent<Spell>());
    }
	
	// Update is called once per frame
	void Update ()
    {
        //  Organize wall
        for (int i = 0; i < walls.GetLength(0); i++)
            for (int j = 0; j < walls.GetLength(1); j++)
                for (int k = 0; k < walls.GetLength(2); k++)
                {
                    if (walls[i, j, k])
                    {
                        if (walls[i, j, k].GetComponent<Rigidbody>() == null)
                        {
                            walls[i, j, k].transform.localPosition = Vector3.Lerp(walls[i, j, k].transform.localPosition, new Vector3(i - walls.GetLength(0) / 2, j, k - walls.GetLength(2) / 2), Time.deltaTime * 3.0f);

                            //  if wall is in place
                            if (Vector3.Distance(walls[i, j, k].transform.localPosition, new Vector3(i - walls.GetLength(0) / 2, j, k - walls.GetLength(2) / 2)) < .1f)
                            {
                                //  Check connection to other wall segments
                                bool connected = false;
                                if ((i + 1 < walls.GetLength(0) && walls[i + 1, j, k] && !walls[i + 1, j, k].GetComponent<Rigidbody>()) &&
                                (i - 1 >= 0 && walls[i - 1, j, k] && !walls[i - 1, j, k].GetComponent<Rigidbody>()))
                                    connected = true;

                                else if (j - 1 >= 0  && walls[i, j - 1, k] && !walls[i, j - 1, k].GetComponent<Rigidbody>())
                                    connected = true;

                                else if ((k + 1 < walls.GetLength(2) && walls[i, j, k + 1] && !walls[i, j, k + 1].GetComponent<Rigidbody>()) &&
                                (k - 1 >= 0 && walls[i, j, k - 1] && !walls[i, j, k - 1].GetComponent<Rigidbody>()))
                                    connected = true;

                                if (j == 0)
                                    connected = true;

                                //  if it isn't connected, make it a rigidbody
                                if (!connected)
                                    walls[i, j, k].AddComponent<Rigidbody>();
                            }
                        }
                    }
                }
    }
}
